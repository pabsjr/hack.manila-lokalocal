import React from "react"
import Waypoint from "react-waypoint"

import Link from "../Link"
import styled from "styled-components"

const Menu = styled.ul`
  display: table;
  margin: auto;
  li {
    display: inline-block;
    a {
      cursor: pointer;
      padding: 10px;
      text-decoration: none;
      color: #000;
      &.active {
        font-weight: 500;
        color: #ac9259;
      }
    }
  }
`

class Navigation extends React.Component {
  constructor(props) {
    super(props)
  }

  /**
   * Render items of the main menu
   */
  renderMenuItems = data => {
    const { handleNav, navClass } = this.props
    const { ID, slug, title } = data

    let config = {
      route: "handler",
      params: { slug },
      url: `/${slug}`
    }

    if (slug === "home") {
      config.url = "/"
      config.route = "home"
      config.params = {}
    }

    return (
      <li className={navClass} key={`${ID}-${slug}`} onClick={handleNav}>
        <Link
          activeClassName="active"
          route={config.route}
          params={config.params}
        >
          <a className="nav-link">{title}</a>
        </Link>
      </li>
    )
  }

  render() {
    const { data, handleNav, enter, navClass } = this.props

    return (
      <React.Fragment>
        <Menu>
          {data.map((post, handleNav) => this.renderMenuItems(post, handleNav))}
        </Menu>
      </React.Fragment>
    )
  }
}

export default Navigation
