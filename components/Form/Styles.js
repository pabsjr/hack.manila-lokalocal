import styled from "styled-components"
import { FormGroup, FormControl } from "react-bootstrap"

const StyledFormControl = styled(FormControl)`
  &.form-control {
    border-radius: 5px;
    padding: 14px;
    line-height: 13px;
    font-size: 15px;
    min-width: 300px;
    border: 1px solid #cccccc;
    width: 100%;
    box-sizing: border-box;

    &:focus {
      /* border: #ccc; */
      /* box-shadow: 0 0 0 0.2rem rgba(156, 159, 162, 0.2); */
    }
    &.input-error {
      border: 1px solid #ff6348;
      &:focus {
        box-shadow: 0 0 0 0.2rem rgba(255, 114, 90, 0.25);
      }
    }
  }
`

const SubmitBtn = styled.button`
  width: 279px;
  padding: 10px;
  background-color: #597a43;
  font-size: 15px;
  color: #fff;
  text-transform: uppercase;
  border: 0;
  outline: none;
  cursor: pointer;
`

const StyledFormGroup = styled(FormGroup)`
  &.form-group {
    margin-bottom: 11px;
    textarea {
      min-height: 140px;
    }
  }
`
const ButtonContainer = styled.div`
  display: flex;
  justify-content: flex-end;
`

const FormError = styled.div`
  font-size: 12px;
  margin-bottom: 12px;
  color: #ff6348;
`

export {
  StyledFormControl,
  StyledFormGroup,
  FormError,
  SubmitBtn,
  ButtonContainer
}
