import React, { Component } from "react"
import { ControlLabel, HelpBlock } from "react-bootstrap"
import { StyledFormControl, StyledFormGroup, FormError } from "./Styles"
import classNames from "classnames"

const FieldGroup = ({ id, label, help, touched, error, type, ...props }) => {
  const inputClass = classNames({
    "input-error": touched && error
  })

  const FormControlProps = {
    className: inputClass,
    ...props
  }

  return (
    <StyledFormGroup controlId={id}>
      {label && <ControlLabel>{label}</ControlLabel>}

      {type === "textarea" ? (
        <StyledFormControl componentClass="textarea" {...FormControlProps} />
      ) : (
        <StyledFormControl {...FormControlProps} />
      )}

      {help && <HelpBlock>{help}</HelpBlock>}
    </StyledFormGroup>
  )
}

class InputField extends Component {
  render() {
    const {
      input,
      label,
      placeholder,
      type,
      showError,
      meta: { touched, error }
    } = this.props

    const FieldGroupProps = {
      id: label,
      type,
      placeholder,
      touched,
      error,
      ...input
    }

    return (
      <React.Fragment>
        <FieldGroup {...FieldGroupProps} />
        {showError && (
          <FormError>{touched && error && <span>{error}</span>}</FormError>
        )}
      </React.Fragment>
    )
  }
}
export default InputField
