import React, { Component } from "react"
import { ControlLabel, HelpBlock } from "react-bootstrap"
import { StyledFormControl, StyledFormGroup, FormError } from "./Styles"
import classNames from "classnames"

const FieldGroup = ({
  id,
  label,
  help,
  touched,
  error,
  type,
  data,
  placeholder,
  ...props
}) => {
  const inputClass = classNames({
    "input-error": touched && error
  })

  const FormControlProps = {
    className: inputClass,
    componentClass: type,
    ...props
  }

  const renderOptions = option => {
    return (
      <option key={option.key} value={option.key}>
        {option.value}
      </option>
    )
  }

  return (
    <StyledFormGroup controlId={id}>
      {label && <ControlLabel>{label}</ControlLabel>}

      <StyledFormControl {...FormControlProps}>
        <option value="">{placeholder}</option>
        {data.map(renderOptions)}
      </StyledFormControl>

      {help && <HelpBlock>{help}</HelpBlock>}
    </StyledFormGroup>
  )
}

class SelectField extends Component {
  render() {
    const {
      input,
      label,
      placeholder,
      type,
      showError,
      data,
      meta: { touched, error }
    } = this.props

    const FieldGroupProps = {
      id: label,
      type,
      placeholder,
      touched,
      error,
      data,
      ...input
    }

    return (
      <React.Fragment>
        <FieldGroup {...FieldGroupProps} />
        {showError && (
          <FormError>{touched && error && <span>{error}</span>}</FormError>
        )}
      </React.Fragment>
    )
  }
}
export default SelectField
