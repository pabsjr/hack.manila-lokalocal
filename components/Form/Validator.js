import { createValidator } from "revalidate";

const isValidEmail = createValidator(
  message => value => {
    if (value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)) {
      return message;
    }
  },
  field => `${field} is not valid`
);

const isRequired = createValidator(
  message => value => {
    if (value == null || value === "") {
      return message;
    }
  },
  field => `${field} is required`
);

export { isValidEmail, isRequired };
