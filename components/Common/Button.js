import React, { Fragment } from "react"
import PropTypes from "prop-types"
import styled from "styled-components"

const AppButton = styled.div`
  border: 1px solid #fff;
  background: #FFF;
  padding: 20px;
  margin: 10px;
`

class Button extends React.Component {
  render() {
    const { children, ...rest } = this.props
    

    return(
      <AppButton {...rest}>{children}</AppButton>
    )
  }
}

export default Button