import React from "react"
import { H1 } from "./styles"

class Title extends React.Component {
  render() {
    const { children, className } = this.props
    return <H1 className={className}>{children}</H1>
  }
}

export default Title
