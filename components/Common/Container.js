import React from "react"
import { PageContainer } from "./styles"

export default class Container extends React.Component {
  render() {
    const { children } = this.props
    return <PageContainer className="page-container">{children}</PageContainer>
  }
}
