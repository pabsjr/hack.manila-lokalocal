import React, {Fragment} from "react"
import { connect } from "react-redux"
import { withRouter } from "next/router"
import Sidebar from "react-sidebar";

import Dimension from '../Dimension'
import AppSidebar from "../Sidebar"
import Header from "../Header";

export default options => {
  let _options = options

  options = {
    header: true,
    ..._options
  }

  options.getInitialProps = (context) => {
    const extraProps = {
      // ua: context.req.headers['user-agent']
    }
    return _options.getInitialProps(context, extraProps)
  }

  return Component => createPage(options, Component)
}

const createPage = (options, InnerComponent) => {
  class Page extends React.Component {

    constructor(props){
      super(props)
      this.state = {
        isMobile: false,
        md: '',
        doRenderContent: false,
        sidebarOpen: false
      }
      this.onSetSidebarOpen = this.onSetSidebarOpen.bind(this)
    }

    /**
     * Toggle sidebar
     * @param {*} open 
     */
    onSetSidebarOpen(open) {
      this.setState({ sidebarOpen: open });
    }

    /**
     * Render Mobile view
     */
    renderMobile() {
      console.log("props", this.props)
      const { cart } = this.props
      const { sidebarOpen } = this.state
      const sidebarProps = {
        sidebar: <AppSidebar/>,
        open: sidebarOpen,
        defaultSidebarWidth: 300,
        touchHandleWidth: 40,
        onSetOpen: this.onSetSidebarOpen,
        pullRight: true,
        styles: {sidebar: { background: "#201a17", padding: "40px", minWidth: "210px" }}
      }
      return(
        <Fragment>
            <Sidebar {...sidebarProps}>
              <Header cart={cart} />
              <button onClick={() => this.onSetSidebarOpen(true)}>
                Open sidebar
              </button>
              <InnerComponent {...this.props} />
            </Sidebar>
        </Fragment>
      )
    }

    renderDesktop() {
      return (
        "we are mobile only"
      )
    }

    /**
     * Handle changes on the viewport size.
     * 
     * @return {void}
     */
    onSize = ({ width }) => {
      const { maxWidth } = this.props

      this.setState({
        isMobile: width <= maxWidth,
        md: this.props.md,
        doRenderContent: true
      })
    }

    render() {
      const { isMobile, doRenderContent } = this.state
      // const {ua} = this.props
      // const md = new MobileDetect(ua)
      // const isMobile = md.mobile() || md.tablet

      return (
        <React.Fragment>
          <Dimension onSize={this.onSize} />
          {doRenderContent && (isMobile ? this.renderMobile() : this.renderDesktop()) }
        </React.Fragment>
      )
    }
  }

  Page.getInitialProps = options.getInitialProps

  Page.defaultProps = {
    maxWidth: 768
  }

  const mapStateToProps = ({ site }) => ({
    isNavOpen: site.isNavOpen,
    cart: site.items
  })

  const mapDispatchToProps = dispatch => ({
    setActiveClass: value =>
      dispatch({
        type: "MENU",
        payload: value
      })
  })

  Page = connect(
    mapStateToProps,
    mapDispatchToProps
  )(Page)

  return withRouter(Page)
}
