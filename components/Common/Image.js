import React from "react"

export default class Image extends React.Component {
  render() {
    const { url } = this.props
    return <img src={url} />
  }
}
