import React from "react"
import styled from "styled-components"

import { SocialShare, FooterLine } from "../index"
import { device } from "../../utils/Device"
import SiteLink from "../Link"
import siteConfig from "../../utils/config"

const LinkWrapper = styled.div`
  display: flex;
  align-items: center;

  a {
    text-decoration: none;
    color: inherit;
  }

  @media ${device.tablet} {
    align-items: flex-start;
    flex-direction: column;
  }
`

const ShareWrapper = styled.div`
  padding-right: 88px;
`

class Link extends React.Component {
  renderLink() {
    const { title, slug, type } = this.props

    if (slug && title) {
      return (
        <FooterLine>
          <SiteLink route="single-post" params={{ type, slug }}>
            <a className="nav-link">view {title}</a>
          </SiteLink>
        </FooterLine>
      )
    }
  }

  render() {
    const { config, slug, type } = this.props
    const { baseURL } = siteConfig
    const postUrl = `${baseURL}/${type}/${slug}`
    return (
      <React.Fragment>
        <LinkWrapper>
          {config && (
            <ShareWrapper>
              <SocialShare allowed={config} url={postUrl} />
            </ShareWrapper>
          )}

          {this.renderLink()}
        </LinkWrapper>
      </React.Fragment>
    )
  }
}

export default Link
