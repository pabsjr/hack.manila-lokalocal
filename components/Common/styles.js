import styled from "styled-components"

import variables from "../../utils/variables"
import { device } from "../../utils/Device"

const H1 = styled.h1`
  font-size: 30px;
  color: ${variables.color.primary};
  letter-spacing: ${variables.letterSpacing(30, 20)}px;
  line-height: ${variables.lineHeight(30, 50)};
  font-family: ${variables.font.primary};
  margin: 0;
  padding: 0;
`

const Content = styled.p`
  font-size: 18px;
  color: ${variables.color.secondary};
  letter-spacing: ${variables.letterSpacing(18, 200)}px;
  line-height: ${variables.lineHeight(18, 25)};
  font-family: ${variables.font.secondary};
  margin: 0;
  padding: 0;
  max-height: 144px;
  overflow: hidden;
  display: -webkit-box;
  -webkit-line-clamp: 6;
  -webkit-box-orient: vertical;
  text-overflow: ellipsis;
  text-align: justify;
`

const HR = styled.span`
  max-width: 142px;
  height: 2px;
  background-color: #ac9259;
  display: block;
  width: 142px;
  margin-right: 20px;
`

const LinkTitle = styled.div`
  font-size: 15px;
  color: ${variables.color.primary};
  letter-spacing: ${variables.letterSpacing(15, 20)}px;
  font-family: ${variables.font.primary};
  font-weight: 800;
  text-transform: lowercase;
`

const TitleWrapper = styled.div`
  margin-right: 0;
  display: flex;
  align-items: center;
  @media ${device.tablet} {
    flex-wrap: wrap;
    width: 100%;
    justify-content: flex-end;
    margin-top: 20px;
  }
`

const PageContainer = styled.div`
  margin: 57px auto;
`

export { H1, H2, HR, Content, LinkTitle, TitleWrapper, PageContainer }
