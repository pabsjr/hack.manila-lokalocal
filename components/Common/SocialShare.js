import React from "react"
import styled from "styled-components"
import { FacebookProvider, ShareButton } from "react-facebook"

import { LinkTitle } from "./styles"

const SocialShareWrapper = styled.div``

const IconsList = styled.ul`
  margin: 0;
  padding: 0;
  list-style: none;
  display: flex;
  justify-content: flex-end;
  li {
    display: inline-flex;
    margin-left: 19px;
    align-items: center;
    &:first-child {
      margin-left: 0;
    }
    &:last-child {
      margin-left: 15px;
    }
    a {
      display: block;
      text-decoration: none;
    }
    button {
      background: transparent;
      border: 0;
      cursor: pointer;
      outline: none;
    }
    img {
      width: 41px;
      height: 41px;
      vertical-align: middle;
    }
  }
`

const icons = [
  {
    name: "share",
    icon: "../../static/images/icons/share-icon.png"
  },
  {
    name: "fb",
    icon: "../../static/images/icons/fb-icon.png"
  },
  {
    name: "top",
    icon: "../../static/images/icons/back-to-top.png"
  },
  {
    name: "label",
    text: "share"
  }
]

class SocialShare extends React.Component {
  getEnabledSocialShare = () => {
    const { icons, allowed } = this.props

    let sites = icons.filter(icon => allowed[icon.name])
    return sites
  }

  /**
   *
   */
  renderSocialShare = (site, index) => {
    const { links, url } = this.props

    if (site.name === "label") {
      return (
        <li key={site.name}>
          <a href="#">
            <LinkTitle>{site.text}</LinkTitle>
          </a>
        </li>
      )
    }

    if (site.name === "fb") {
      return (
        <li key={site.name}>
          <FacebookProvider appId="359493467942170">
            <ShareButton href={url}>
              <img src={site.icon} />
            </ShareButton>
          </FacebookProvider>
        </li>
      )
    }

    if (links) {
      return (
        <li key={site.name}>
          <a href={site.name}>
            <img src={site.icon} />
          </a>
        </li>
      )
    }

    return (
      <li key={site.name}>
        <a href="#">
          <img src={site.icon} />
        </a>
      </li>
    )
  }

  render() {
    const sites = this.getEnabledSocialShare()

    return (
      <SocialShareWrapper>
        <IconsList>
          {sites.map((props, index) => this.renderSocialShare(props, index))}
        </IconsList>
      </SocialShareWrapper>
    )
  }
}

SocialShare.defaultProps = {
  icons
}

export default SocialShare
