import React from "react"
import styled from "styled-components"

import { device } from "../../utils/Device"

const SiteHR = styled.hr`
  background-color: #ac9259;
  max-width: 798px;
  border: 0;
  height: 2px;
  margin: 0 auto;
  @media ${device.tablet} {
    margin: 0 40px;
  }
`

class HR extends React.Component {
  render() {
    return <SiteHR />
  }
}

export default HR
