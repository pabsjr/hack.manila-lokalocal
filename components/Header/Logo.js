class Logo extends React.Component {
  render() {
    const { url, title, className } = this.props
    return <img className={className} src={url} alt={title} />
  }
}

export default Logo
