import styled from "styled-components"

import variables from "../../utils/variables"
import Logo from "./Logo"
import { device } from "../../utils/Device"

const SiteLogo = styled.span`
  img {
    width: 41px;
    height: 41px;
    vertical-align: middle;
  }
`

const LogoWrapper = styled.div`
  display: flex;
  align-items: center;
`

const SiteTitle = styled.h1`
  margin: 0;
  font-family: ${variables.font.primary};
  font-size: 15px;
  text-transform: uppercase;
  color: #597a43;
  letter-spacing: ${variables.letterSpacing(15, 100)}px;
  margin-left: 19px;

  @media ${device.tablet} {
    margin-left: 10px;
    font-size: 13px;
  }

  @media ${device.mobileL} {
    font-size: 11px;
  }
`

class Brand extends React.Component {
  render() {
    const { logo, title, withTitle } = this.props

    const logoProps = {
      url: logo,
      title
    }

    return (
      <React.Fragment>
        <LogoWrapper>
          <SiteLogo>
            <Logo {...logoProps} />
          </SiteLogo>
          {withTitle ? <SiteTitle>{title}</SiteTitle> : ""}
        </LogoWrapper>
      </React.Fragment>
    )
  }
}

export default Brand
