import React, {Fragment} from "react"

class Header extends React.Component {
  render() {
    const { cart } = this.props
    return (
      <Fragment>
      <h1>Header</h1>
      <div className="Nav-item Nav-cart">
          <span className="fa fa-cart-plus"></span> Cart [{cart.length}]
      </div>        
      </Fragment>
    )
  }
}

Header.defaultProps = {
  cart: 0
}

export default Header