import styled from "styled-components"

const NavContainer = styled.div`
  text-align: center;
  .bar1,
  .bar2,
  .bar3 {
    width: 35px;
    height: 2px;
    background-color: #597a43;
    margin: 6px 0;
    transition: 0.4s;
  }

  .change .bar1 {
    transform: rotate(-45deg) translate(-6px, 4px);
  }

  .change .bar2 {
    opacity: 0;
  }

  .change .bar3 {
    transform: rotate(45deg) translate(-6px, -6px);
  }
`

export { NavContainer }
