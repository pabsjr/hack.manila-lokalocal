import React from "react"
import { connect } from "react-redux"
import { CalculateSig, GenerateSignature } from "../../utils/GravityForm"
import ContactForm from "../ContactUs/ContactForm"
import api from "../../utils/api"
import config from "../../utils/config"
import styled from "styled-components"

const FormContainer = styled.div`
  display: table;
  width: 100%;
`
class ContactUs extends React.Component {
  constructor(props) {
    super(props)
    this.submitForm = this.submitForm.bind(this)
  }
  /**
   *  Get form details from WP Gravity Form plugin
   *
   * @param {string} method
   * @param {string} route
   * @memberof ContactUs
   */
  async sendForm(method, route, data) {
    const { gravity } = config
    const future_unixtime = gravity.unixtime + gravity.expiration

    const stringToSign = GenerateSignature(
      gravity.publicKey,
      method,
      route,
      future_unixtime
    )

    const sig = CalculateSig(stringToSign, gravity.privateKey)
    const result = await api.Form.send(
      route,
      gravity.publicKey,
      sig,
      future_unixtime,
      data
    )
    this.props.submit(result)
  }

  /**
   * Send form method
   * @param {*} values
   */
  submitForm(values) {
    const config = {
      method: "POST",
      route: "forms/1/submissions"
    }

    this.sendForm(config.method, config.route, values)
  }

  render() {
    const { form } = this.props
    const contactFormProps = {
      submitForm: this.submitForm,
      details: form
    }
    return (
      <React.Fragment>
        <FormContainer className="page__contact-us-form">
          <ContactForm {...contactFormProps} />
        </FormContainer>
      </React.Fragment>
    )
  }
}

const mapDispatchToProps = dispatch => ({
  submit: status => {
    return dispatch({ type: "SUBMIT_FORM", payload: status })
  }
})

export default connect(
  null,
  mapDispatchToProps
)(ContactUs)
