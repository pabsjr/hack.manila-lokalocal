import { composeValidators, combineValidators } from "revalidate";

import { isValidEmail, isRequired } from "../Form/Validator";

const validate = combineValidators({
  input_1: composeValidators(isRequired("Name"))(),
  input_2: composeValidators(isRequired("Address"))(),
  input_3: composeValidators(isRequired("City"))(),
  input_7: composeValidators(isRequired("Email"), isValidEmail("Email"))(),
  input_5: composeValidators(isRequired("Email Message"))()
});

export default validate;
