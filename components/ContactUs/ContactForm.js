import React, { Component } from "react"
import { Field, reduxForm } from "redux-form"

import InputField from "../Form/InputField"
import validate from "./Validate"
import { ButtonContainer, SubmitBtn } from "../Form/Styles"

class ContactForm extends Component {
  /**
   * Render each field from Gravity form wp plugin
   *
   * @memberof ContactForm
   */
  renderFormFields = field => {
    return (
      <Field
        key={`input_${field.id}`}
        name={`input_${field.id}`}
        type={field.type}
        component={InputField}
        placeholder={field.placeholder}
        showError={true}
      />
    )
  }

  /**
   * Submit Redux form
   *
   * @memberof ContactForm
   */
  requestSubmitForm = values => {
    this.props.submitForm(values)
  }

  render() {
    const { details, handleSubmit } = this.props

    if (details.response) {
      const { fields } = details.response
      return (
        <React.Fragment>
          <form onSubmit={handleSubmit(this.requestSubmitForm)}>
            <div>
              {fields.map(this.renderFormFields)}
              <ButtonContainer>
                <SubmitBtn className="btn-submit" type="submit">
                  Send
                </SubmitBtn>
              </ButtonContainer>
            </div>
          </form>
        </React.Fragment>
      )
    }
    return <div />
  }
}

ContactForm = reduxForm({
  form: "contact",
  destroyOnUnmount: false,
  validate
})(ContactForm)

export default ContactForm
