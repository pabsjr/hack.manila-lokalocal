import React from "react"
import PropTypes from "prop-types"
import posed from 'react-pose';
import styled from "styled-components"

import ItemContent from "./Content"

const Content = posed.div({
  closed: { height: 0 },
  open: { height: 'auto' }
})

const Item = styled.div`

  .content {
    overflow: hidden;
    font-size: 18px;
    background: rgba(0, 0, 0, 0.8);
  }

  .content-wrapper {
    padding: 50px 20px;
    color: #FFF;
  }

`

class Product extends React.Component {


  handleItemEvents = () => {
    const { setActiveItem, index } = this.props
    setActiveItem(index)
  }

  render(){
    const { item, index, setActiveItem, open, addToCart } = this.props

    return(
      <Item> 
        <h2 className="title" onClick={this.handleItemEvents}>
          {item.first_name}
        </h2>
        <Content className="content" pose={open === index ? 'open' : 'closed'}>
          {open === index && <ItemContent addToCart={addToCart} content={item} />}
        </Content>
      </Item>
    )
  }
}

Product.propTypes = {
  item: PropTypes.object,
  index: PropTypes.number,
  setActiveItem: PropTypes.func,
  open: PropTypes.any
}

export default Product