import React, {Fragment} from "react"
import PropTypes from "prop-types"
import posed from 'react-pose';
import { connect } from "react-redux"

import withShoppingCart from "./withShoppingCart"
import Product from "./Product"

const Content = posed.div({
  closed: { height: 0 },
  open: { height: 'auto' }
});

class Products extends React.Component {
  state = { open: false }

  setActiveItem = (i) => {
    this.setState({ open: open === i ? false : i })
  }

  /**
   * Add item to the cart
   */
  handleAddToCart = (product) => {
    const { add } = this.props
    add(product)
    console.log("handleAddToCart", product)
  }
  
  /**
   * Render Product item
   * @param {*} item 
   * @param {*} index 
   * @param {*} open 
   */
  renderProduct(item, index, open) {

    const itemProps = {
      key: `item-${item.id}`,
      item,
      setActiveItem: this.setActiveItem,
      addToCart: this.handleAddToCart,
      open,
      index
    }

    return <Product {...itemProps} />
  }

  render() {
    const { open } = this.state
    const { data } = this.props

    console.log("props", this.props)

    return (
      <Fragment>
        {data.map((item, index) => (this.renderProduct(item, index, open)))}
      </Fragment>
    );
  }
}


Products.propTypes = {
  data: PropTypes.arrayOf(PropTypes.object)
}

export default withShoppingCart(Products)
