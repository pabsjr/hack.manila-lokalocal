import React, {Fragment} from "react"
import { connect } from "react-redux"

export default (InnerComponent) => {
  class Page extends React.Component {
 
    componentWillReceiveProps(props, next) {
      console.log(props)
    }

    render() {
      return(
        <Fragment>
          <InnerComponent {...this.props} />
        </Fragment>
      )
    }
  }

  const mapStateToProps = ({ site }) => ({
    cart_items: site.items
  })
  
  const mapDispatchToProps = dispatch => ({
    add: status => {
      return dispatch({ type: "ADD_TO_CART", payload: status })
    }
  })

  Page = connect(
    mapStateToProps,
    mapDispatchToProps
  )(Page)

  return Page
}
