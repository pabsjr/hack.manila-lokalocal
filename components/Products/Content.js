import React, { Fragment } from "react"
import PropTypes from "prop-types"

import Button from "../Common/Button"

class Content extends React.Component {

  state = {
    added: false
  }

  render() {
 
    const { content, addToCart } = this.props

    return(
      <Fragment>
        <h1>{content.first_name}</h1>
        <Button onClick={() => addToCart(content)}>Add to cart</Button>
      </Fragment>
    )
  }
}

Content.propTypes = {
  content: PropTypes.object
}

export default Content