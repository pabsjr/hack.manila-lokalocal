import React, {Fragment} from "react"
import styled from "styled-components"

import Profile from "./Profile"
import Navigation from "./Navigation"

class AppSidebar extends React.Component {
  render() {
    return (
      <Fragment>
        <Profile />
        <Navigation />
      </Fragment>
    )
  }
}

export default AppSidebar