import React, {Fragment} from "react"
import styled from "styled-components"

import variables from "../../utils/variables"

const User = styled.div`
  display: flex;
  color: #FFF;
  align-items: center;
`

const Details = styled.div`
  padding-left: 10px;
`

const Photo = styled.div`
  height: 60px;
  width: 60px;
  border-radius: 50%;
  overflow: hidden;

  img {
    max-width: 100%;
  }
`
const Name = styled.div`
  font-size: 15px;
  font-weight: 700;
`

const Rank = styled.div`
  color: ${variables.color.primary};
  font-size: 10px;
  font-weight: 500;
`

class Profile extends React.Component {
  render() {
    return (
      <User>
        <Photo>
          <img src="https://dummyimage.com/600x600/000/fff" />
        </Photo>
        <Details>
          <Name>Patrick Dr.</Name>
          <Rank>Coffee Addicted Pro</Rank>
        </Details>
      </User>
    )
  }
}

export default Profile