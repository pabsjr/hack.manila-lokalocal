import React, { Fragment } from "react"
import styled from "styled-components"

import Link from "../Link"

const Nav = styled.ul`
  list-style: none;
  margin: 0;
  padding: 0;
  margin-top: 20%;

  li {
    &.active {
      margin-bottom: 5px;
      margin-top: 5px;
      margin-left: -40px;
      margin-right: -40px;

      a {
        background: #008cc9;
        border-radius: 0;
        padding: 15px 40px;
        box-shadow: 3px 4px 20px 0px #040404;
      }

    }

    a {
      font-weight: 200;
      padding: 13px 0;
      display: block;
      color: #FFF;
      font-size: 14px;
      text-decoration: none;
    }

    span {
      font-size: 14px;
      margin-right: 9px;
    }

  }
`

class Navigation extends React.Component {
  render() {
    return (
      <Fragment>
        <Nav>
            <li>
              <Link activeClassName="active" href="/">
                <a className="nav-link"><span className="lnr lnr-home"></span> Dashboard</a>
              </Link> 
            </li> 
            <li>
              <Link activeClassName="active" href="/shop">
                <a className="nav-link"><span className="lnr lnr-coffee-cup"></span> Menu</a>
              </Link> 
            </li>
            <li><a><span className="lnr lnr-chart-bars"></span> Nutrition</a></li>
            <li><a><span className="lnr lnr-user"></span> Membership</a></li>
        </Nav>
      </Fragment>
    )
  }
}

export default Navigation