import React from "react"
import fetch from "isomorphic-unfetch"

import createPage from "../components/Common/decorator"
import Products from "../components/Products";

class Shop extends React.Component {

  render() {
    const { products } = this.props

    return(
      <Products data={products.data} />
    )
  }
}

const getInitialProps = async (context, {ua}) => {
  const result = await fetch("https://reqres.in/api/users?page=1")
  const products = await result.json()

  return { ua, products }
}

export default createPage({
  getInitialProps
})(Shop)
