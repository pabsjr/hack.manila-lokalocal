import React, {Fragment} from "react"
import createPage from "../components/Common/decorator"
// import fetchPage from "../api/requests/fetchPage"

class Home extends React.Component {

  render() {
    console.log("test", this.props)
    return(
      <Fragment>
      This is a test
      </Fragment>
    )
  }
}

const getInitialProps = async (context, {ua}) => {
  return { ua }
}

export default createPage({
  getInitialProps
})(Home)
