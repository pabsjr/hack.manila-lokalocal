import CryptoJS from "crypto-js"

const CalculateSig = (stringToSign, privateKey) => {
  //calculate the signature needed for authentication
  const hash = CryptoJS.HmacSHA1(stringToSign, privateKey)
  const base64 = hash.toString(CryptoJS.enc.Base64)
  return encodeURIComponent(base64)
}

const GenerateSignature = (publicKey, method, route, future_unixtime) => {
  return publicKey + ":" + method + ":" + route + ":" + future_unixtime
}

export { CalculateSig, GenerateSignature }
