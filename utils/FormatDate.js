const formatDate = date => {
  const formatDate = new Date(date);
  if (formatDate) {
    const options = {
      year: "numeric",
      month: "long",
      day: "numeric"
    };
    return new Intl.DateTimeFormat("en-US", options).format(formatDate);
  }
  return date;
};

export default formatDate;
