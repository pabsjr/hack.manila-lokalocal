import superagentPromise from "superagent-promise"
import _superagent from "superagent"
import config from "./config"

const superagent = superagentPromise(_superagent, global.Promise)

const API_ROOT = config.api.baseURL

const responseBody = res => res.body

const requests = {
  get: url => superagent.get(`${API_ROOT}${url}`).then(responseBody),
  post: (url, data) => {
    return superagent
      .post(`${API_ROOT}${url}`)
      .set("Accept", "application/json")
      .send(JSON.stringify({ input_values: data }))
      .then(function(res) {
        return res.body
      })
  }
}

// http://192.168.99.100:8080/gravityformsapi/forms/1?api_key=170c018b82&signature=wVTvsLthremGq9NImQuZeG%2BN90I%3D&expires=1525539150
const Form = {
  contact: (route, publicKey, signature, expiration) =>
    requests.get(
      `/gravityformsapi/${route}/?api_key=${publicKey}&signature=${signature}&expires=${expiration}`
    ),
  send: (route, publicKey, signature, expiration, data) =>
    requests.post(
      `/gravityformsapi/${route}/?api_key=${publicKey}&signature=${signature}&expires=${expiration}`,
      data
    )
}

export default {
  Form
}
