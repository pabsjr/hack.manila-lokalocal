import ContactUs from "../templates/ContactUs"
import AboutUs from "../templates/AboutUs"
import Services from "../templates/Services"

const templates = [
  {
    template: "about-us",
    component: AboutUs
  },
  {
    template: "services",
    component: Services
  },
  {
    template: "contact-us",
    component: ContactUs
  }
]
export default templates
