const size = {
  mobileS: "320px",
  mobileM: "376px",
  mobileL: "425px",
  tablet: "768px",
  tabletL: "992px",
  laptop: "1172px",
  laptopL: "1440px",
  desktop: "2560px"
}

export const device = {
  mobileS: `(min-width: ${size.mobileS})`,
  mobileM: `(max-width: ${size.mobileM})`,
  mobileL: `(max-width: ${size.mobileL})`,
  tablet: `(max-width: ${size.tablet})`,
  tabletL: `(max-width: ${size.tabletL})`,
  laptop: `(max-width: ${size.laptop})`,
  laptopL: `(min-width: ${size.laptopL})`,
  desktop: `(min-width: ${size.desktop})`,
  desktopL: `(min-width: ${size.desktop})`
}
