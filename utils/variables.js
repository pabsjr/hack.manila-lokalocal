const variables = {
  font: {
    primary: "'Rubik', sans-serif",
    secondary: "Brandon Grotesque"
  },
  color: {
    primary: "#2897d7",
    secondary: "#AC9259"
  },
  letterSpacing: (size, spacing) => {
    return (size * spacing) / 1000
  },
  lineHeight: (size, height) => {
    return height / size
  }
}

export default variables
