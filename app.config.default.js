const app = {
  name: "Lokalocal",
  port: 3000,
  url: `http://api.test.local/`
}

module.exports = {
  name: app.name,
  port: app.port,
  baseURL: `http://localhost:${app.port}`,
  api: {
    url: app.url,
    baseURL: `${app.url}wp-json`
  },
  fb: {
    app: "410735536117897"
  }
}
