const path = require('path')
const SWPrecacheWebpackPlugin = require('sw-precache-webpack-plugin')
const webpack = require('webpack')
const _merge = require('lodash/merge')
const defaultAppConfig = require('./app.config.default')

module.exports = {
	webpack: (config, { dev }) => {
		const oldEntry = config.entry

		config.entry = () =>
			oldEntry().then(entry => {
				entry['main.js'] && entry['main.js'].push(path.resolve('./utils/offline'))
				return entry
			})

		/* Enable only in Production */
		if (!dev) {
			// Service Worker
			config.plugins.push(
				new SWPrecacheWebpackPlugin({
					cacheId: 'next-ss',
					filepath: './static/sw.js',
					minify: true,
					staticFileGlobsIgnorePatterns: [/\.next\//],
					staticFileGlobs: [
						'static/**/*' // Precache all static files by default
					],
					runtimeCaching: [
						// Example with different handlers
						{
							handler: 'fastest',
							urlPattern: /[.](png|jpg|css)/
						},
						{
							handler: 'networkFirst',
							urlPattern: /^http.*/ //cache all files
						}
					]
				})
			)
		}

		let appConfig = require(path.join(process.cwd(), 'app.config.js'))
		let defaultConfig = require(path.join(process.cwd(), 'app.config.default'))
		
		// Merge default paths
		appConfig = _merge({}, defaultConfig, appConfig);

		config.plugins.push(
			new webpack.DefinePlugin({
				APP_CONFIG: JSON.stringify(appConfig)
			})
		)

		return config
  }
}