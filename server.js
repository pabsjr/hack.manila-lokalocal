const { createServer } = require("http")
const path = require("path")
const next = require("next")
const routes = require("./routes")
const config = require("./app.config")

const dev = process.env.NODE_ENV !== "production"
const app = next({ dev: process.env.NODE_ENV !== "production" })
const handler = routes.getRequestHandler(app)

const PORT = config.port

app.prepare().then(_ => {
  const server = createServer((req, res) => {
    if (req.url === "/sw.js" || req.url === "/robots.txt") {
      app.serveStatic(req, res, path.resolve("./static" + req.url))
    } else if (req.url === "/favicon.ico") {
      app.serveStatic(req, res, path.resolve("./static" + req.url))
    } else {
      handler(req, res)
    }
  })

  server.listen(PORT, err => {
    if (err) throw err
    console.log(`> ${config.name} - running on port ${PORT}`)
  })
})
