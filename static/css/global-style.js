import styled from "styled-components"
import { createGlobalStyle } from "styled-components"
import variables from "../../utils/variables"
import { device } from "../../utils/Device"

const GlobalStyle = createGlobalStyle`
  .container{
    max-width: calc(1440px - 268px);
    margin: auto;
    position: relative;
    
    @media ${device.laptop} {
      margin: 0 40px;
    }
     
    @media ${device.mobileL} {
      margin: 0 20px;
    }
  }
  body{
  color: #000;
   padding: 0;
   font-family: ${variables.font.primary};
   -webkit-font-smoothing: antialiased;
   -moz-osx-font-smoothing: grayscale;
   margin: 0;
   overflow-x: hidden;
  }
  .header {
    position: relative;
    z-index: 1050;
  }
  strong {
    font-weight: 500;
  }
  h1, h2, h3, h4, h5, h6 { 
    color: ${variables.color.primary}; 
    font-family: ${variables.font.primary};
  }
  .animated-page {
    transition: opacity 0.2s ease-in;
  }

  
`
export default GlobalStyle
