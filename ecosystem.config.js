const config = require("./app.config")

module.exports = {
  apps: [
    {
      name: config.name,
      script: "server.js",
      instances: "max",
      exec_mode: "cluster",
      env: {
        PORT: config.port,
        NODE_ENV: "development"
      },
      env_production: {
        PORT: config.port,
        NODE_ENV: "production"
      }
    }
  ]
}
