
const initialValue = {
  isNavOpen: false,
  content: {
    form: {},
    gravityForm: {},
    gravity: {}
  },
  items: []
}

const site = (state = initialValue, action) => {
  switch (action.type) {
    case "MENU":
      return { ...state, isNavOpen: action.payload }

    case "LOAD_FORM":
      return {
        ...state,
        content: {
          form: action.payload
        }
      }

    case "ADD_TO_CART":
      console.log("add1", action.payload)
      return {
        ...state,
        items: [...state.items, action.payload]
      }
      
    default:
      return state
  }
}

export default site
