import fetch from "isomorphic-unfetch"
import config from "../../utils/config"
import fetchContactForm from "./fetchContactForm"

export default async (type, slug) => {
  const { api } = config
  let result

  try {
    let [header, data, footer, site, form] = await Promise.all([
      fetch(`${api.baseURL}/pages/location/main-menu`),
      fetch(`${api.baseURL}/post/${type}/${slug}`),
      fetch(`${api.baseURL}/pages/location/footer-menu`),
      fetch(`${api.baseURL}/site/info`),
      fetchContactForm(1)
    ])

    result = {
      primary: await header.json(),
      footer: await footer.json(),
      site: await site.json(),
      data: await data.json(),
      form: await form.json(),
      ...result
    }
  } catch (err) {}

  return result
}
