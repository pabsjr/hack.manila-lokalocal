import fetch from "isomorphic-unfetch"
import config from "../../utils/config"

export default async slug => {
  const { api } = config
  const response = await fetch(`${api.baseURL}/page/${slug}`)

  return await response.json()
}
