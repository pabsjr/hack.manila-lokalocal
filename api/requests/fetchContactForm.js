import { CalculateSig, GenerateSignature } from "../../utils/GravityForm"
import config from "../../utils/config"
import fetch from "isomorphic-unfetch"

const fetchContactForm = id => {
  const { api, gravity } = config
  const formConfig = {
    method: "GET",
    route: `forms/${id}`
  }
  const future_unixtime = gravity.unixtime + gravity.expiration

  const stringToSign = GenerateSignature(
    gravity.publicKey,
    formConfig.method,
    formConfig.route,
    future_unixtime
  )

  const signature = CalculateSig(stringToSign, gravity.privateKey)

  return fetch(
    `${api.url}/gravityformsapi/${formConfig.route}/?api_key=${
      gravity.publicKey
    }&signature=${signature}&expires=${future_unixtime}`
  )
}
export default fetchContactForm
